import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.parser.nbp.MathCurrencyStatistic;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class MathCurrencyStatisticTest {
    private MathCurrencyStatistic mathCurrencyStatistic;

    @DataProvider(name="avrgInput")
    public static Object[][] invalidInput(){
        List<BigDecimal> avrg = new ArrayList<>();
        avrg.add(new BigDecimal("1.5"));
        avrg.add(new BigDecimal("3.5"));
        avrg.add(new BigDecimal("7.0"));

        return new Object[][] {{avrg, new BigDecimal("4.0")}};
    }

    @DataProvider(name="deviationInput")
    public static Object[][] deviationInput(){
        List<BigDecimal> deviation = new ArrayList<>();
        deviation.add(new BigDecimal("1.1"));
        deviation.add(new BigDecimal("2.2"));
        deviation.add(new BigDecimal("3.3"));

        return new Object[][] {{deviation, new BigDecimal("2.2")}};
    }

    @BeforeTest
    public void setUp(){
        mathCurrencyStatistic = new MathCurrencyStatistic();
    }

    @Test(dataProvider = "avrgInput")
    public void shouldFindAvrg(List<BigDecimal> avrg, BigDecimal expected){
        Assert.assertEquals(mathCurrencyStatistic.findAverage(avrg), expected);
    }

    @Test(dataProvider = "deviationInput")
    public void shouldFindDeviation(List<BigDecimal> deviation, BigDecimal expected){
        Assert.assertEquals(mathCurrencyStatistic.findAverage(deviation), expected);
    }
}
