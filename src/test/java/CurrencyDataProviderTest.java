import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.parser.nbp.CurrencyDataProvider;
import pl.parser.nbp.enums.CurrencyType;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

public class CurrencyDataProviderTest {
    private CurrencyDataProvider currencyDataProvider;

    @DataProvider(name="expectedOutput")
    public static Object[][] expectedOutput(){
        List<BigDecimal> buy = new ArrayList<>();
        List<BigDecimal> sell = new ArrayList<>();

        buy.add(new BigDecimal("3.6929"));
        buy.add(new BigDecimal("3.6802"));

        sell.add(new BigDecimal("3.7675"));
        sell.add(new BigDecimal("3.7546"));

        return new Object[][] {{buy, sell}};
    }

    @BeforeTest
    private void setUp(){
        currencyDataProvider = new CurrencyDataProvider(CurrencyType.USD, "2016-04-04", "2016-04-05");
    }

    @Test(dataProvider = "expectedOutput")
    public void shouldReturnCorrectData(List<BigDecimal> buy, List<BigDecimal> sell){
        List<BigDecimal> resultBuy = currencyDataProvider.getBuyPriceHistory();
        List<BigDecimal> resultSell = currencyDataProvider.getSellPriceHistory();

        Assert.assertEquals(buy.size(), resultBuy.size());
        for(int i = 0; i < resultBuy.size(); i++){
            Assert.assertTrue(buy.get(i).compareTo(resultBuy.get(i)) == 0);
        }

        Assert.assertEquals(sell.size(), resultSell.size());
        for(int i = 0; i < resultSell.size(); i++){
            Assert.assertTrue(sell.get(i).compareTo(resultSell.get(i)) == 0);
        }
    }
}
