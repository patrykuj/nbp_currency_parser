import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import pl.parser.nbp.InputValidator;
import pl.parser.nbp.exceptions.WrongCurrencyException;
import pl.parser.nbp.exceptions.WrongDateFormatException;
import pl.parser.nbp.exceptions.WrongDateRange;
import pl.parser.nbp.exceptions.WrongInputLengthException;

public class InputValidatorTest {
    @DataProvider(name="validInputTest")
    public static Object[][] validInput(){
        return new Object[][] {{"EUR", "2013-01-28", "2013-01-31", true},
                {"USD", "2013-01-28", "2013-01-31", true},
                {"CHF", "2013-01-28", "2013-01-31", true},
                {"GBP", "2013-01-28", "2013-01-31", true}
        };
    }

    @DataProvider(name="invalidInputTest")
    public static Object[][] invalidInput(){
        return new Object[][] {{"ASD", "2013-01-28", "2013-01-31"},
                {"USD", "2001-01-28", "2013-01-31"},
                {"USD", "2013-01-28", "3000-01-31"},
                {"USD", "2013-01-28", "2011-01-31"},
                {"USD", "2005-01-28", "2013-01-31"}
        };
    }

    @Test(dataProvider = "validInputTest")
    public void shouldValidInput(String currency, String startDate, String endDate, Object expected){
        String[] input = {currency, startDate, endDate};
        Assert.assertEquals(InputValidator.validate(input), expected);
    }

    @Test(expectedExceptions = {WrongCurrencyException.class, WrongDateFormatException.class, WrongDateRange.class, WrongInputLengthException.class}, dataProvider = "invalidInputTest")
    public void shouldReportInvalidInput(String currency, String startDate, String endDate){
        String[] input = {currency, startDate, endDate};
        InputValidator.validate(input);
    }
}
