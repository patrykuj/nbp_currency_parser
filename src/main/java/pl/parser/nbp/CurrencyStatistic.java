package pl.parser.nbp;

import java.math.BigDecimal;
import java.util.List;

public interface CurrencyStatistic {
    BigDecimal findAverage(List<BigDecimal> values);
    BigDecimal findDeviation(List<BigDecimal> values);
}
