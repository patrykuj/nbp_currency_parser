package pl.parser.nbp.exceptions;

public class WrongDateRange extends RuntimeException {
    public WrongDateRange(String message){
        super(message);
    }
}
