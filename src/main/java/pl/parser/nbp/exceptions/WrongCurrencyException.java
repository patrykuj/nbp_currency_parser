package pl.parser.nbp.exceptions;

public class WrongCurrencyException extends RuntimeException {
    public WrongCurrencyException(String message){
        super(message);
    }
}
