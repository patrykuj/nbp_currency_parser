package pl.parser.nbp.exceptions;

public class WrongInputLengthException extends RuntimeException{
    public WrongInputLengthException(String message){
        super(message);
    }
}
