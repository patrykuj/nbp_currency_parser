package pl.parser.nbp.exceptions;

public class WrongDateFormatException extends RuntimeException {
    public WrongDateFormatException(String message){
        super(message);
    }
}
