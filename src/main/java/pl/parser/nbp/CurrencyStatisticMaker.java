package pl.parser.nbp;

import pl.parser.nbp.enums.CurrencyType;

import java.math.BigDecimal;
import java.util.List;

public class CurrencyStatisticMaker {
    private CurrencyStatistic mathCurrencyStatistic;
    private List<BigDecimal> buyPriceHistory;
    private List<BigDecimal> sellPriceHistory;

    public CurrencyStatisticMaker(String currency, String startDate, String endDate){
        this.mathCurrencyStatistic = new MathCurrencyStatistic();
        CurrencyDataProvider currencyDataProvider = new CurrencyDataProvider(CurrencyType.valueOf(currency), startDate, endDate);
        this.buyPriceHistory = currencyDataProvider.getBuyPriceHistory();
        this.sellPriceHistory = currencyDataProvider.getSellPriceHistory();
    }

    public BigDecimal getAverage(){
        return mathCurrencyStatistic.findAverage(buyPriceHistory);
    }

    public BigDecimal getDeviation(){
        return mathCurrencyStatistic.findDeviation(sellPriceHistory);
    }
}
