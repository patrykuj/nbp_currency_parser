package pl.parser.nbp;

import java.math.BigDecimal;
import java.math.MathContext;
import java.util.List;

public class MathCurrencyStatistic implements CurrencyStatistic{
    @Override
    public BigDecimal findAverage(List<BigDecimal> values) {
        BigDecimal sum = new BigDecimal(0.0);
        for(BigDecimal singleData : values) {
            sum = sum.add(singleData);
        }
        BigDecimal itemCount = new BigDecimal(values.size());
        return sum.divide(itemCount, BigDecimal.ROUND_HALF_EVEN);
    }

    @Override
    public BigDecimal findDeviation(List<BigDecimal> values) {
        BigDecimal mean = findMean(values);
        BigDecimal squareSum = new BigDecimal(0.0);
        BigDecimal powTmp;
        for(BigDecimal price : values){
            powTmp = (price.subtract(mean)).pow(2);
            squareSum = squareSum.add(powTmp);
        }
        BigDecimal subResult = squareSum.divide(new BigDecimal(values.size()), BigDecimal.ROUND_HALF_EVEN);
        return findSqrt(subResult);
    }

    private BigDecimal findMean(List<BigDecimal> values){
        BigDecimal sum = new BigDecimal(0.0);
        for(BigDecimal value : values){
            sum = sum.add(value);
        }
        BigDecimal itemCount = new BigDecimal(values.size());
        return sum.divide(itemCount, BigDecimal.ROUND_HALF_EVEN);
    }

    private BigDecimal findSqrt(BigDecimal value){
        BigDecimal subResult = new BigDecimal(Math.sqrt(value.doubleValue()), MathContext.DECIMAL64);
        BigDecimal bigDecimalTWO = new BigDecimal(2.0);
        int desirePrecision = 128;
        int temPrecision = 16;
        while (desirePrecision > temPrecision){
            subResult = subResult.subtract(subResult.multiply(subResult).subtract(value).
                    divide(subResult.multiply(bigDecimalTWO), desirePrecision, BigDecimal.ROUND_HALF_EVEN));
            temPrecision *= 2;
        }
        return subResult.setScale(4, BigDecimal.ROUND_HALF_EVEN);
    }
}
