package pl.parser.nbp;

import com.sun.org.apache.xerces.internal.impl.xs.opti.DefaultDocument;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pl.parser.nbp.enums.CurrencyType;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.math.BigDecimal;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class CurrencyDataProvider {
    private static final String URLBEGIN = "http://api.nbp.pl/api/exchangerates/rates/c/";
    private static final String URLSEPARATOR = "/";
    private static final String URLEND = "/?format=xml";

    private final String urlAddress;

    private List<BigDecimal> buyPriceHistory;
    private List<BigDecimal> sellPriceHistory;

    public CurrencyDataProvider(CurrencyType currencyType, String startDate, String endDate) {
        this.urlAddress = prepareURL(currencyType, startDate, endDate);
        this.buyPriceHistory = new ArrayList<>();
        this.sellPriceHistory = new ArrayList<>();
        this.createCurrencyDataset();
    }

    public List<BigDecimal> getBuyPriceHistory(){
        return buyPriceHistory;
    }

    public List<BigDecimal> getSellPriceHistory(){
        return sellPriceHistory;
    }

    private void createCurrencyDataset(){
        Document doc = getPricesFromAPI();
        NodeList buyRates = doc.getElementsByTagName("Bid");
        NodeList sellRates = doc.getElementsByTagName("Ask");
        int nodeListLength = buyRates.getLength();
        for(int i = 0; i < nodeListLength; i++){
            BigDecimal buyPrice = new BigDecimal(buyRates.item(i).getTextContent());
            BigDecimal sellPrice = new BigDecimal(sellRates.item(i).getTextContent());
            buyPriceHistory.add(buyPrice);
            sellPriceHistory.add(sellPrice);
        }
    }

    private Document getPricesFromAPI(){
        Document pricesDocument = new DefaultDocument();
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder;
        try {
            documentBuilder = documentBuilderFactory.newDocumentBuilder();
            pricesDocument = documentBuilder.parse(new URL(urlAddress).openStream());
        } catch (IOException | SAXException | ParserConfigurationException e) {
            e.printStackTrace();
        }
        return pricesDocument;
    }

    private String prepareURL(CurrencyType currencyType, String startDate, String endDate){
        return URLBEGIN + currencyType.getCurrencyName() + URLSEPARATOR + startDate + URLSEPARATOR + endDate + URLEND;
    }
}
