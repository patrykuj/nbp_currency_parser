package pl.parser.nbp;

import java.math.BigDecimal;

public class MainClass {
    public static void main(String... args){
        InputValidator.validate(args);

        CurrencyStatisticMaker currencyStatisticMaker = new CurrencyStatisticMaker(args[0], args[1], args[2]);
        BigDecimal average = currencyStatisticMaker.getAverage();
        BigDecimal deviation = currencyStatisticMaker.getDeviation();
        System.out.println(average + "\n" + deviation);
    }
}
