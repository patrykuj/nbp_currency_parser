package pl.parser.nbp;

import pl.parser.nbp.enums.CurrencyType;
import pl.parser.nbp.enums.Messages;
import pl.parser.nbp.exceptions.WrongCurrencyException;
import pl.parser.nbp.exceptions.WrongDateFormatException;
import pl.parser.nbp.exceptions.WrongDateRange;
import pl.parser.nbp.exceptions.WrongInputLengthException;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.concurrent.TimeUnit;

public class InputValidator {
    public static boolean validate(String... userInput){
        if(userInput.length != 3){
            throw new WrongInputLengthException(Messages.InvalidLength.getMessage());
        }
        else if(!getAllCurrencyTypes().contains(userInput[0].toUpperCase())){
            throw new WrongCurrencyException(Messages.InvalidCurrency.getMessage());
        }
        else if(!userInput[1].matches("([0-9]{4})-([0-9]{2})-([0-9]{2})") && !userInput[2].matches("([0-9]{4})-([0-9]{2})-([0-9]{2})")){
            throw new WrongDateFormatException(Messages.WrongDateFormat.getMessage());
        }
        else if(!isDatesRangesValid(userInput[1], userInput[2])){
            throw new WrongDateFormatException(Messages.WrongDateFormat.getMessage());
        }
        else if(!isTimePeriodValid(userInput[1], userInput[2])){
            throw new WrongDateRange(Messages.WrongDateRange.getMessage());
        }
        else {
            return true;
        }
    }

    private static Set<String> getAllCurrencyTypes(){
        Set<String> currencyTypes = new HashSet<>();
        for(CurrencyType currency : CurrencyType.values()){
            currencyTypes.add(currency.getCurrencyName());
        }
        return currencyTypes;
    }

    private static boolean isDatesRangesValid(String start, String end){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date today = new Date();
        Date dateStart = getDate(start);
        Date dateEnd = getDate(end);
        Date dataBaseStartDate = getDate("2002-01-01");

        if (!start.equals(simpleDateFormat.format(dateStart)) && !end.equals(simpleDateFormat.format(dateEnd))) {
            return false;
        }
        else if(!dateStart.before(dateEnd) || !dateStart.after(dataBaseStartDate)){
            return false;
        }
        return !dateEnd.after(today);
    }

    private static boolean isTimePeriodValid(String startDate, String endDate){
        Date dateStart = getDate(startDate);
        Date dateEnd = getDate(endDate);
        long timeInterval = getDateDiff(dateStart, dateEnd, TimeUnit.DAYS);
        return timeInterval < 93;
    }

    private static long getDateDiff(Date start, Date end, TimeUnit timeUnit) {
        long diffInMillies = end.getTime() - start.getTime();
        return timeUnit.convert(diffInMillies, TimeUnit.MILLISECONDS);
    }

    private static Date getDate(String date){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        Date result;
        try {
            result = simpleDateFormat.parse(date);
        } catch (ParseException e) {
            throw new WrongDateFormatException(Messages.WrongDateFormat.getMessage());
        }
        return result;
    }
}
