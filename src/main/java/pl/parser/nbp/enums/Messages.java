package pl.parser.nbp.enums;

public enum Messages {
    InvalidLength   ("Wrong amount of arguments. Should be given 3 arguments:[USD/EUR/CHF/GBP] " +
                     "[Start date in format(year-month-day) [End date in format(year-month-day)]"),
    InvalidCurrency ("Given currency not found. Should be given 1 currency type from list:[USD/EUR/CHF/GBP]"),
    WrongDateFormat ("Given data is wrong format. Should be given in format:[year-month-day]"),
    WrongDateRange  ("Given dates extends 93 days period");

    private final String message;
    Messages(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }
}
