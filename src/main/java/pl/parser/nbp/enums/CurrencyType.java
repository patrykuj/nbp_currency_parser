package pl.parser.nbp.enums;

public enum CurrencyType {
    USD("USD"), EUR("EUR"), CHF("CHF"), GBP("GBP");

    private final String currencyName;
    CurrencyType(String currencyName){
        this.currencyName = currencyName;
    }

    public String getCurrencyName() {
        return currencyName;
    }
}
